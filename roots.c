#include<stdio.h>
#include<math.h>
void main()
{
    float a,b,c,root1,root2;
    float real,imag,dis;
    printf("enter the value of a,b and c of given quadratic equation\n");
    scanf("%f%f%f",&a,&b,&c);
    if(a==0||b==0||c==0)
    {
    printf("incorrect quadratic equation\n");
    }
    else
    {
    dis=b*b-4*a*c;
    if(dis<0)
    {
    printf("imaginary roots\n");
    real=-b/(2*a);
    imag=sqrt(abs(dis))/(2*a);
    printf("root1=%f+i%f\n",real,imag);
    printf("root2=%f-i%f\n",real,imag);
    }
    else if(dis==0)
    {
    printf("the roots are real and equal\n");
   root1=-b/(2*a);
   root2=root1;
   printf("root1=%f\n",root1);
   printf("root2=%f\n",root2);
   }
   else 
   {
   printf("the roots are realand distict\n");
   root1=(-b+sqrt(dis))/(2*a);
   root2=(-b-sqrt(dis))/(2*a);
   printf("root1=%f\n",root1);
   printf("root2=%f\n",root2);
   }
   }
   }